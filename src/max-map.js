export default function maxMap (array, mappingFunc) {
  // TODO:
  //   This function will find the maximum mapped value of `array`. The mapped value
  //   for each item should be calculated using `mappingFunc`.
  //
  //   Please read the test to get a basic idea.
  // <-start-

  // filter false value.
  if (!array) {
    return undefined;
  }
  // filter false values in the array.
  const filteredArray = array.filter(item => item);
  if (filteredArray.length === 0) {
    return undefined;
  }
  // use mappingFunc to map the array and get the max mapped value.
  const mappedArray = array.map(item => mappingFunc(item));
  const expected = mappedArray.reduce((pre, cur) => pre > cur ? pre : cur);
  return expected;
  // --end-->
}
