export default function before (n, func) {
  // TODO:
  //   Creates a function that invokes func while it's called less than `n` times.
  //   Please read the test to get how it works.
  // <-start-
  if (n.isNaN) {
    return () => {};
  }
  let times = 0;
  return () => {
    ++times;
    if (times < n) {
      func();
    }
  };
  // --end-->
}
